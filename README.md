### Running the code

This project is written in ES6 for convenience, speed and longevity. To run this project, open Chrome 50+ 
as this is currently the only browser with 95%+ ES6 support.

### Purpose

This is the server-side instance of the game. This is the sole point of truth and any game logic is calculated 
here and sent to the connected players. It's important that the server logic is done first as doing this on the 
client's machine opens the game up to cheating.

### Why is there a UI if this is Server-Side Code?

We are using a basic 2D UI as a visual representation of the game to test things are running as expected.


### How do I contribute?

That's awesome that you want to help! Contributing is easy; just pick an item from the TODO's list below and start 
coding. When you are happy with what you have done, send a pull request and we'll review and integrate.

### TODO's

##### Units movement, attack and special attack logic:

* Scout 
* Assassin
* Cleric
* Pyromancer
* Enchantress
* Dragonspeaker Mage
* Dark Magic Witch
* Lightening Ward
* Barrier Ward
* Mud Golem
* Golem Ambusher
* Frost Golem
* Stone Golem
* Dragon Tyrant
* Berserker
* Beast Rider
* Poison Wisp
* Furgon

##### Websockets

The plan is to use websockets for the game, lobby and chat. We are thinking about using [socket.io](http://socket.io/) 
but we are open to other suggestions.

##### Lobby

This is where players can talk to each other and put their avatar down to wait for a game.

##### Player setup

This is where the player can change the setup of their play characters.
