class Furgon extends Unit {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        super({
            name: 'Furgon',
            Game: props.Game,
            playerId: props.playerId,
            tileIndex: props.tileIndex,
            hp: 48,
            power: 'summon',
            armor: 0,
            blocking: {
                front: 50,
                side: 25,
                back: 0
            },
            recovery: 1,
            movement: 3,
            blockable: false,
            movable: true // Moves out of the way for friendly units
        });
    }

}
