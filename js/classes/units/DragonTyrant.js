class DragonTyrant extends Unit {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        super({
            name: 'DragonTyrant',
            Game: props.Game,
            playerId: props.playerId,
            tileIndex: props.tileIndex,
            hp: 68,
            power: 28,
            armor: 16,
            blocking: {
                front: 40,
                side: 20,
                back: 0
            },
            recovery: 3,
            movement: 4,
            blockable: false,
            movable: false // Moves out of the way for friendly units
        });
    }

}
