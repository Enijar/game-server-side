class FrostGolem extends Unit {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        super({
            name: 'FrostGolem',
            Game: props.Game,
            playerId: props.playerId,
            tileIndex: props.tileIndex,
            hp: 60,
            power: 'paralyze',
            armor: 0,
            blocking: {
                front: 0,
                side: 0,
                back: 0
            },
            recovery: 2,
            movement: 2,
            blockable: false,
            movable: true // Moves out of the way for friendly units
        });
    }

}
