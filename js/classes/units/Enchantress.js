class Enchantress extends Unit {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        super({
            name: 'Enchantress',
            Game: props.Game,
            playerId: props.playerId,
            tileIndex: props.tileIndex,
            hp: 35,
            power: 'paralyze',
            armor: 0,
            blocking: {
                front: 0,
                side: 0,
                back: 0
            },
            recovery: 3,
            movement: 3,
            blockable: false,
            movable: true // Moves out of the way for friendly units
        });
    }

}
