class Scout extends Unit {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        super({
            name: 'Scout',
            Game: props.Game,
            playerId: props.playerId,
            tileIndex: props.tileIndex,
            hp: 40,
            power: 18,
            armor: 8,
            blocking: {
                front: 60,
                side: 30,
                back: 0
            },
            recovery: 2,
            movement: 4,
            blockable: true,
            movable: true // Moves out of the way for friendly units
        });
    }

}
