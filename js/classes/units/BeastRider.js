class BeastRider extends Unit {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        super({
            name: 'BeastRider',
            Game: props.Game,
            playerId: props.playerId,
            tileIndex: props.tileIndex,
            hp: 38,
            power: 19,
            armor: 15,
            blocking: {
                front: 45,
                side: 22,
                back: 0
            },
            recovery: 1,
            movement: 4,
            blockable: true,
            movable: true // Moves out of the way for friendly units
        });
    }

}
