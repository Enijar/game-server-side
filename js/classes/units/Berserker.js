class Berserker extends Unit {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        super({
            name: 'Berserker',
            Game: props.Game,
            playerId: props.playerId,
            tileIndex: props.tileIndex,
            hp: 42,
            power: 22,
            armor: 0,
            blocking: {
                front: 25,
                side: 12,
                back: 0
            },
            recovery: 1,
            movement: 3,
            blockable: true,
            movable: true // Moves out of the way for friendly units
        });
    }

}
