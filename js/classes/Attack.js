class Move {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        applyProps(this, props);
        
        this.attackableTiles = [];
    }

    /**
     * Get attackable tile indexes and add them to the attackableTiles array.
     *
     * @param {Number|undefined} index (recursive method, it's set itself)
     * @param {Number=0} step
     */
    findPath(index = undefined, step = 0) {

    }

    getAttackableTiles() {
        return this.attackableTiles;
    }

}
